var spawn = require('child_process').spawn
  , read = require('read')
  , fs = require('fs')
  , path = require('path')
  , render = require(path.join(__dirname, 'templates'))
  , keypair = require('keypair');

exports = module.exports = function(relPath){

  if(!relPath) relPath = ".";

  fs.exists(path.join(relPath,'private-key.pem'), function (exists) {
    if (exists)
      promptOverwrite();
    else
      generateKeys();
  });

  function promptOverwrite(){
    read({
      prompt: render('keys-exist-overwrite'),
      default: "n"
    }, function(er, overwrite){
      if(overwrite.toLowerCase() === "y")
        generateKeys();
    });
  }

  function generateKeys(){
    var key = keypair({
      bits: 2048
    });
    fs.writeFileSync(path.join(relPath,'public-key.pem'), key.public);
    fs.writeFileSync(path.join(relPath,'private-key.pem'), key.private);
  }

}