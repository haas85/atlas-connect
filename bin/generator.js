var fs = require("fs")
  , temp = require('temp')
  , https = require("https")
  , AdmZip = require('adm-zip')
  , read = require('read')
  , rimraf = require('rimraf')
  , path = require('path')
  , render = require(path.join(__dirname, 'templates'))
  , ProgressBar = require('progress')
  , ncp = require('ncp').ncp
  , es = require('event-stream')
  , slug = require('slug');


exports = module.exports = function(name, template, cb){
  var dlUrl = "https://bitbucket.org/atlassian/atlassian-connect-express-template/get/";
  var getStartedUrl = "http://j.mp/get-started-with-atlassian-connect-express";

  if (template) {
    if (template.toLowerCase() === "hipchat") {
      dlUrl += "hipchat.zip";
      getStartedUrl = "http://j.mp/atlassian-connect-express-hipchat";
    } else if (template.toLowerCase() === "hipchat-ack") {
      dlUrl += "hipchat.zip";
      getStartedUrl = "https://bitbucket.org/atlassianlabs/ac-koa-hipchat";
    } else if (template.search(/https?:/) === 0) {
      // template is a URL
      dlUrl = template;
    } else {
      console.log(render('template-not-found', {template: template}));
      return;
    }
  } else {
    // default to whatever is on master
    dlUrl += "master.zip";
  }

  https.get(dlUrl, function(res){
    var data = [], dataLen = 0;

    var len = parseInt(res.headers['content-length'] || 1000, 10);
    var bar = new ProgressBar('Downloading template [:bar] :percent :etas', {
        complete: '='
      , incomplete: ' '
      , width: 20
      , total: len
    });

    res.on('data', function(chunk){
      data.push(chunk);
      dataLen += chunk.length;
      bar.tick(chunk.length);
    }).on('end', function(){
      if (res.statusCode >= 500) {
        console.log(render('server-down', {code: res.statusCode}));
        return;
      }
      if (res.statusCode == 404) {
        console.log(render('template-not-found', {template: template}));
        return;
      }
      var buf = new Buffer(dataLen);

      for (var i=0, len = data.length, pos = 0; i < len; i++) {
          data[i].copy(buf, pos);
          pos += data[i].length;
      }

      var zip = new AdmZip(buf);
      var zipEntries = zip.getEntries();
      var zipDir = "";

      console.log("Downloading template [===================] 100% 0.0s");

      fs.exists(name,function(err){
        if (err){
          read({
            prompt: render('project-exists-overwrite',{name: name}),
            default: "n"
          }, function(er, overwrite){
            if(overwrite.toLowerCase() === "y"){
              rimraf(name,function(err){
                if(!err){
                  createScaffold();
                }
              });
            }
          });
        } else {
          createScaffold();
        }

        function createScaffold(){
          var outDir = path.join(process.cwd(),name);
          temp.mkdir(name, function(err,dirPath){
            zip.extractAllTo(dirPath, true);
            zipEntries.forEach(function(zipEntry){
              zipEntriesArry = zipEntry.entryName.split('/');
              var filepath = [name, zipEntriesArry
                .slice(1).join('/')].join('/');
              console.log(("  " + filepath).yellow);
            });
            ncp(path.join(dirPath, zipEntriesArry[0]), outDir, { transform: updateDescriptorKeys }, function (err) {
              console.log(render('project-created',{
                name: name,
                getStartedUrl: getStartedUrl
              }));
              cb(err);
            });
          });
        }

        function updateDescriptorKeys(read, write){
          if (read.path.split('/').slice(-1)[0].indexOf('atlassian-connect.json') === 0) {
            var slugname = slug(name);
            read
              .pipe(es.replace('hello-world', slugname))
              .pipe(es.replace('my-add-on', slugname))
              .pipe(write);
          } else {
            read.pipe(write);
          }
        }
      });
    });
  });
};
